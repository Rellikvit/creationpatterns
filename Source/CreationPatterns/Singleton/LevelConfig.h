// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LevelConfig.generated.h"

UCLASS()
class ULevelConfig : public UObject
{
	GENERATED_BODY()
	
public:	
	
	ULevelConfig();

	static ULevelConfig* GetInstance();

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "ConfigInstance")
	bool ValuableGlobalBoolVar;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "ConfigInstance")
	float ValuableGlobalFloatVar;
	
private:
	
	static ULevelConfig* Instance;
};

inline ULevelConfig* ULevelConfig::GetInstance()
{
	if(!Instance)
	{
		Instance = NewObject<ULevelConfig>();
	}
	return Instance;
}
