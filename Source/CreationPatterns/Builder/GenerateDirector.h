// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CreationPatterns/Builder/BiomeGenerate.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GenerateDirector.generated.h"

UCLASS()
class CREATIONPATTERNS_API UGenerateDirector : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static void GenerateForestBiome(IBiomeGenerate* Generator);

	static void GeneratePlainsBiome(IBiomeGenerate* Generator);
};
