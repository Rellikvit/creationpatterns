// Fill out your copyright notice in the Description page of Project Settings.


#include "PlainsBiomeGenerator.h"

UObject* UPlainsBiomeGenerator::GetResult() const
{
	return BiomeHeart;
}

void UPlainsBiomeGenerator::GenerateGrounds()
{
	SpawnPlains();
}

void UPlainsBiomeGenerator::GenerateStructures()
{
	SpawnDungeons();
}

void UPlainsBiomeGenerator::SpawnHeart()
{
	//Spawn Biome Root Actor
}

void UPlainsBiomeGenerator::SpawnPlains()
{
	//Spawn Plains
}

void UPlainsBiomeGenerator::SpawnDungeons()
{
	//Spawn Dungeons
}
