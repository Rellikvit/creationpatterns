// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BiomeGenerate.h"
#include "ForestBiomeGenerator.generated.h"

UCLASS()
class CREATIONPATTERNS_API UForestBiomeGenerator : public UObject, public IBiomeGenerate
{
	GENERATED_BODY()

public:
	virtual UObject* GetResult() const override;
	
	virtual void GenerateGrounds() override;
	virtual void GenerateObstacles() override;

protected:
	void SpawnHeart();
	void SpawnGrass();
	void SpawnTrees();
	
private:
	UPROPERTY(meta = (AllowPrivateAccess = "true"), BlueprintReadOnly,VisibleAnywhere,Category="Biome")
	AActor* BiomeHeart {nullptr};
	
};
