// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CreationPatterns/Builder/BiomeGenerate.h"
#include "GameFramework/Actor.h"
#include "MapGenerator.generated.h"

UCLASS()
class CREATIONPATTERNS_API AMapGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	void GenerateMap();
};

template<class TClass>
IBiomeGenerate* CastToBiomeGenerator(TClass* Object)
{
	return static_cast<IBiomeGenerate*>(Object->GetInterfaceAddress(UBiomeGenerate::StaticClass()));
}
