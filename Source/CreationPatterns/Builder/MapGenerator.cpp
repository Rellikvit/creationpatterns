
#include "MapGenerator.h"

#include "GenerateDirector.h"
#include "CreationPatterns/Builder/ForestBiomeGenerator.h"
#include "CreationPatterns/Builder/PlainsBiomeGenerator.h"

void AMapGenerator::GenerateMap()
{
	auto ForestGenerator = NewObject<UForestBiomeGenerator>(this);
	auto IGenerator = CastToBiomeGenerator(ForestGenerator);
	UGenerateDirector::GenerateForestBiome(IGenerator);

	auto PlainsGenerator = NewObject<UPlainsBiomeGenerator>(this);
	IGenerator = CastToBiomeGenerator(PlainsGenerator);
	UGenerateDirector::GenerateForestBiome(IGenerator);
}

