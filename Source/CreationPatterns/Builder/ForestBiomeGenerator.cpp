// Fill out your copyright notice in the Description page of Project Settings.


#include "ForestBiomeGenerator.h"

UObject* UForestBiomeGenerator::GetResult() const
{
	return BiomeHeart;
}

void UForestBiomeGenerator::GenerateGrounds()
{
	SpawnGrass();
}

void UForestBiomeGenerator::GenerateObstacles()
{
	SpawnTrees();
}

void UForestBiomeGenerator::SpawnHeart()
{
	//Spawn Biome Root Actor
}

void UForestBiomeGenerator::SpawnGrass()
{
	//Spawn Grass
}

void UForestBiomeGenerator::SpawnTrees()
{
	//Spawn Trees
}
