// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BiomeGenerate.h"
#include "UObject/NoExportTypes.h"
#include "PlainsBiomeGenerator.generated.h"

/**
 * 
 */
UCLASS()
class CREATIONPATTERNS_API UPlainsBiomeGenerator : public UObject, public IBiomeGenerate
{
	GENERATED_BODY()
public:
	virtual UObject* GetResult() const override;
	
	virtual void GenerateGrounds() override;
	virtual void GenerateStructures() override;

protected:
	void SpawnHeart();
	void SpawnPlains();
	void SpawnDungeons();
	
private:
	UPROPERTY(meta = (AllowPrivateAccess = "true"), BlueprintReadOnly,VisibleAnywhere,Category="Biome")
	AActor* BiomeHeart {nullptr};
	
};
