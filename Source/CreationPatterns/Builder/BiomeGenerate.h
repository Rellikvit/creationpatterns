// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BiomeGenerate.generated.h"

/**
 * 
 */
UINTERFACE(MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UBiomeGenerate : public UInterface
{
	GENERATED_BODY()
	
};

class IBiomeGenerate
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category="BiomeGeneration")
	virtual UObject* GetResult() const PURE_VIRTUAL(IBiomeGenerate::GetResult, return nullptr; );

	UFUNCTION(BlueprintCallable, Category="BiomeGeneration")
	virtual void GenerateGrounds() PURE_VIRTUAL(IBiomeGenerate::GenerateGrounds );

	UFUNCTION(BlueprintCallable, Category="BiomeGeneration")
	virtual void GenerateObstacles() PURE_VIRTUAL(IBiomeGenerate::GenerateObstacles );

	UFUNCTION(BlueprintCallable, Category="BiomeGeneration")
	virtual void GenerateStructures() PURE_VIRTUAL(IBiomeGenerate::GenerateStructures );
	
};
