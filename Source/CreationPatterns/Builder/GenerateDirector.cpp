// Fill out your copyright notice in the Description page of Project Settings.


#include "GenerateDirector.h"

void UGenerateDirector::GenerateForestBiome(IBiomeGenerate* Generator)
{
	if(Generator)
	{
		Generator->GenerateGrounds();
		Generator->GenerateObstacles();
	}
}

void UGenerateDirector::GeneratePlainsBiome(IBiomeGenerate* Generator)
{
	if(Generator)
	{
		Generator->GenerateGrounds();
		Generator->GenerateStructures();
	}
}
