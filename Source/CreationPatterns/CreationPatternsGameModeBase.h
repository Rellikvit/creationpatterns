// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CreationPatternsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CREATIONPATTERNS_API ACreationPatternsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
