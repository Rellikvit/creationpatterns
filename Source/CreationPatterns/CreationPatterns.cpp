// Copyright Epic Games, Inc. All Rights Reserved.

#include "CreationPatterns.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CreationPatterns, "CreationPatterns" );
