// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasicEnemy.h"
#include "MeleeEnemy.generated.h"

UCLASS()
class CREATIONPATTERNS_API AMeleeEnemy : public ABasicEnemy
{
	GENERATED_BODY()

public:
	AMeleeEnemy();

protected:
	
	virtual void Move() override;
	
	virtual void Attack() override;

	UFUNCTION(BlueprintCallable)
	virtual void HeavyAttack();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float EnemyArmor;
	
};
