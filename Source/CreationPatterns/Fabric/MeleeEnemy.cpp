
#include "MeleeEnemy.h"

AMeleeEnemy::AMeleeEnemy()
{
	FText t = t.FromString("I'm melee");
	TextComponent->Text = t;
	EnemyDamage = 50;
	EnemyHealth = 100;
	EnemyArmor = 100;
}

void AMeleeEnemy::Move()
{
	//MeleeMove
}

void AMeleeEnemy::Attack()
{
	//MeleeAttack
}

void AMeleeEnemy::HeavyAttack()
{
	//HeavyAttack
}
