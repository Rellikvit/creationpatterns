// Fill out your copyright notice in the Description page of Project Settings.


#include "RangeEnemy.h"

ARangeEnemy::ARangeEnemy()
{
	FText t = t.FromString("I'm range");
	TextComponent->Text = t;
	EnemyDamage = 100;
	EnemyHealth = 50;
	EnemyShield = 50;
}

void ARangeEnemy::Move()
{
	//RangeMove
}

void ARangeEnemy::Attack()
{
	//RangeAttack
}

void ARangeEnemy::LightAttack()
{
	//LightAttack
}
