// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/Actor.h"
#include "BasicEnemy.generated.h"

UCLASS()
class CREATIONPATTERNS_API ABasicEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	ABasicEnemy();

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision")
	USphereComponent* SphereComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Billboard")
	UTextRenderComponent* TextComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	float EnemyDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float EnemyHealth;

	UFUNCTION(BlueprintCallable)
	virtual void Move() PURE_VIRTUAL(ABasicEnemy::Move);

	UFUNCTION(BlueprintCallable)
	virtual void Attack() PURE_VIRTUAL(ABasicEnemy::Attack);

	UFUNCTION(BlueprintCallable)
	void GetEnemyInfo();
	
public:
	virtual void Tick(float DeltaTime) override;
};
