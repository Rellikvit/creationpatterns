// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasicEnemy.h"
#include "RangeEnemy.generated.h"

/**
 * 
 */
UCLASS()
class CREATIONPATTERNS_API ARangeEnemy : public ABasicEnemy
{
	GENERATED_BODY()

public:
	ARangeEnemy();

protected:
	
	virtual void Move() override;
	
	virtual void Attack() override;

	UFUNCTION(BlueprintCallable)
	virtual void LightAttack();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float EnemyShield;
	
};
