// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/Interface.h"
#include "CloneInterface.generated.h"

/**
 * 
 */
UINTERFACE(MinimalAPI,meta=(CannotImplementInterfaceInBlueprint))
class UCloneInterface : public UInterface
{
	GENERATED_BODY()
};

class ICloneInterface
{
	GENERATED_BODY()
public:
	
	virtual void* Clone();
};
