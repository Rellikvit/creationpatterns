
#pragma once

#include "CoreMinimal.h"
#include "CloneInterface.h"
#include "ShopList.generated.h"

/**
 * 
 */
UCLASS()
class CREATIONPATTERNS_API UShopList : public UObject, public ICloneInterface
{
	GENERATED_BODY()

public:
	virtual void* Clone() override;

	UObject* GetShopItem() const
	{
		return ShopItem;
	}

	int GetPrice() const
	{
		return Price;
	}
	
private:
	UObject* ShopItem;

	int Price;
	
};
