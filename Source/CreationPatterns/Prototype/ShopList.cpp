// Fill out your copyright notice in the Description page of Project Settings.


#include "ShopList.h"

void* UShopList::Clone()
{
	auto Copy = NewObject<UShopList>(GetOuter());
	if(IsValid(Copy))
	{
		Copy->Price = GetPrice();
		Copy->ShopItem = GetShopItem();
	}
	return Copy;
}
